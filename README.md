# 4Gewinnt -> rust-cpp

## How to implement a Rust library in a already existing C++ project

### 1. Create a new Rust library

```bash
cargo new --lib rust_lib
```

### 2. Add the following to the Cargo.toml

```toml
[package]
name = "rust_example"
version = "0.1.0"
edition = "2021"
RUST_BACKTRACE=1 # optional for debugging purposes

[lib]
crate-type = ["cdylib"] # Specifies a dynamic library that can be linked with C++

[dependencies]
rand = "0.8.5"
autocxx = "0.26.0"
cxx = "1.0"

[build-dependencies]
autocxx-build = "0.26.0"
miette = { version = "5", features = ["fancy"] }
```

### 3. Make a build.rs file

```rust
fn main() -> miette::Result<()> {
    let path = std::path::PathBuf::from(<set-the-cpp-include-path>); // include path
    let mut b = autocxx_build::Builder::new("src/lib.rs", &[&path]).build()?;
    // This assumes all your C++ bindings are in main.rs
    b.flag_if_supported("-std=c++20").compile("rustTest"); // arbitrary library name, pick anything
    println!("cargo:rerun-if-changed=src/lib.rs");
    // Add instructions to link to any C++ libraries you need.
    Ok(())
}
```

### 4. edit src/lib.rs

```rust
use autocxx::prelude::*; // include autocxx

include_cpp! {
    #include "some-header" // include the header file
    safety!(unsafe_ffi) // use unsafe_ffi
    generate!("some-bindings") // generate the bindings
}

#[no_mangle]
pub extern "C" fn some_function_name() -> ffi::some_return_type {
  todo!() // implement the function
}
```

### 5. Build the library

```bash
cargo build --release
```

### 6. Edit the CMakeLists.txt

```cmake
cmake_minimum_required(VERSION 3.11.0)
project(<PROJECT_NAME> VERSION <PROJECT_VERSION>)

# Include CTest for testing support
include(CTest)
enable_testing()

# Add the external project as a dependency
include(ExternalProject)

ExternalProject_Add(
    <EXTERNAL_PROJECT_NAME>
    SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/<EXTERNAL_PROJECT_DIRECTORY>"
    BINARY_DIR "${CMAKE_CURRENT_SOURCE_DIR}/<EXTERNAL_PROJECT_DIRECTORY>"
    CONFIGURE_COMMAND ""
    BUILD_COMMAND cargo build --release # or any other build command for the external project
    INSTALL_COMMAND ""
    BUILD_BYPRODUCTS "${CMAKE_CURRENT_SOURCE_DIR}/<EXTERNAL_PROJECT_DIRECTORY>/target/release/<EXTERNAL_LIBRARY_NAME>.so" # Adjust the library name if needed
)

# Specify the main executable target
add_executable(<EXECUTABLE_NAME>
    <LIST_OF_EXECUTABLE_SOURCES>
)

# Link against the external library and set dependency on the external project
add_dependencies(<EXECUTABLE_NAME> <EXTERNAL_PROJECT_NAME>)
target_link_libraries(<EXECUTABLE_NAME> PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/<EXTERNAL_PROJECT_DIRECTORY>/target/release/<EXTERNAL_LIBRARY_NAME>.so") # Adjust the library name if needed

# Include directories
target_include_directories(<EXECUTABLE_NAME> PRIVATE
    <LIST_OF_INCLUDE_DIRECTORIES>
)

# Set C++ standard to C++20 and enable additional compiler warnings
target_compile_features(<EXECUTABLE_NAME> PUBLIC cxx_std_20)
target_compile_options(<EXECUTABLE_NAME> PUBLIC -Wall -pedantic -Wextra)

```

### add a header file

```cpp
#pragma once

extern "C" {
Column* some_function_name();
}
```

### use the header file as normal

```cpp
#include "some-header.h"

int main() {
  some_function_name();
}
```

## nice to knows

- You can use the `autocxx::include_cpp!` macro multiple times in one file
- c++ classes and methods are converted to rust structs and functions and can be used as such
- check out the [documentation](https://google.github.io/autocxx/index.html) of autocxx for more information
