fn main() -> miette::Result<()> {
    let path = std::path::PathBuf::from("../include"); // include path
    let mut b = autocxx_build::Builder::new("src/lib.rs", &[&path]).build()?;
    // This assumes all your C++ bindings are in main.rs
    b.flag_if_supported("-std=c++20").compile("rustTest"); // arbitrary library name, pick anything
    println!("cargo:rerun-if-changed=src/lib.rs");
    // Add instructions to link to any C++ libraries you need.
    Ok(())
}
