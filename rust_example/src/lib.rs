use autocxx::prelude::*;
use rand::Rng;

include_cpp! {
    #include "board.hpp"
    #include "column.hpp"
    safety!(unsafe_ffi)
    generate!("Board")
    generate!("Column")
}

#[no_mangle]
pub extern "C" fn rust_main(board: &ffi::Board) -> *mut ffi::Column {
    let mut rand_slot: usize;
    let mut rng = rand::thread_rng();

    loop {
        rand_slot = rng.gen_range(0..=board.columns());

        if !board.is_full(ffi::Column::new(rand_slot).within_unique_ptr()) {
            break;
        }
    }

    let column = ffi::Column::new(rand_slot).within_unique_ptr();

    column.into_raw()
}
