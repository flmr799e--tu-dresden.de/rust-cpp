#pragma once

#include <random>
#include <string>

#include "../include/base_entity.hpp"
#include "../include/board.hpp"

class MirrorAI : public BaseEntity {
public:
  MirrorAI(const std::string &name, Color color, Difficulty difficulty)
      : BaseEntity(name, color, difficulty) {}

  Column choose_slot(const Board &board) override;

private:
};
