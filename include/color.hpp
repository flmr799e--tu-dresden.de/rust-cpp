#pragma once

#include <iostream>
enum class Color { red, yellow, empty };

std::ostream &operator<<(std::ostream &os, const Color &e);

Color operator~(Color e);
