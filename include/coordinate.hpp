#pragma once

#include <iostream>

#include "column.hpp"
#include "row.hpp"

struct Coordinate {
  Row row;
  Column column;

  Coordinate(Row row_, Column column_) : row(row_), column(column_) {}
};
