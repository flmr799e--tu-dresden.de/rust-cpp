#include "medium_ai.hpp"
#include "rust.hpp"
#include <iostream>
#include <typeinfo>

Column MediumAI::choose_slot(const Board &board) {
  Column result = *rust_main(board);

  return result;
}
