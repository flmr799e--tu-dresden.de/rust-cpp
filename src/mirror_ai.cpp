#include "../lib/mirror_ai.hpp"
#include <vector>

Column MirrorAI::choose_slot(const Board &board) {
  auto last_col = board.last_col().value;
  auto columns = board.columns();

  auto slot = Column(columns - last_col - 1);

  if (slot.value >= columns || slot.value < 0) {
    slot = Column(0);
    while (board.is_full(slot)) {
      slot.value++;
      if (slot.value == board.columns()) {
        slot.value = 0;
      }
    }
  }

  return slot;
}
