#include "difficulty.hpp"

std::ostream &operator<<(std::ostream &os, const Difficulty &e) {
  switch (e) {
  case Difficulty::player:
    return os << "player";
    break;
  case Difficulty::baby:
    return os << "baby";
    break;
  case Difficulty::easy:
    return os << "easy";
    break;
  case Difficulty::medium:
    return os << "medium";
    break;
  case Difficulty::hard:
    return os << "hard";
    break;
  }
  return os;
}
