#include "hard_ai.hpp"
#include <iostream>

Column HardAI::when_full(Column slot, const Board &board) {
  while (board.is_full(slot)) {
    choosen_slot_.value++;
    if (choosen_slot_.value == board.columns()) {
      choosen_slot_.value = 0;
    }
    slot = choosen_slot_;
  }
  return slot;
}

Column HardAI::choose_slot(const Board &board) {
  auto last_col = board.last_col();
  auto get_action = rand_num_.generate(0, 3);
  if (first_run_ == true) {
    choosen_slot_ = Column(rand_num_.generate(0, board.columns() - 1));
    first_run_ = false;
  }

  Column slot(0);
  if (get_action == 0) { //  0 --> block horizontal
    slot = last_col;
    slot = when_full(slot, board);
  } else if (get_action == 1) { // 1 --> block vertical
    if (!last_ai_col_.value < !board.columns()) {
      slot = choosen_slot_;
    } else {
      slot = Column(last_col.value++);
    }
    slot = when_full(slot, board);
  } else if (get_action == 2) { // 2 --> place on top of own slot
    slot = choosen_slot_;

  } else if (get_action == 3) { // 3 --> place next to own slot
    if (choosen_slot_.value == 0) {
      slot = Column(choosen_slot_.value++);
    }
    if (choosen_slot_.value == board.columns() - 1) {
      choosen_slot_ = Column(0);
    }
    slot = Column(choosen_slot_.value++);
    slot = when_full(slot, board);
  }

  last_ai_col_ = slot;
  return slot;
}
